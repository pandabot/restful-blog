var async = require('async');
var express = require('express');
var fs = require('fs');
var marked = require('marked');

var app = express();

/*
   RESTful API Config
*/

app.configure(function () {
	app.use(express.logger('dev'));
	app.use(express.json());
});

/*
   Globals
*/

var sort_by = function(field, reverse, primer){
   var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};

   reverse = [-1, 1][+!!reverse];

   return function (a, b) {
      return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
   }
}

/*
	Marked Config
*/

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  smartypants: false
});

/*
	Blog/News API
*/

app.get('/info/blog', function(req, res) {
   var blogJson = [];

   var staticJson = __dirname + '/static/info/blog/json/';
   var staticFiles = __dirname + '/static/info/blog/_source/';

   async.forEach(fs.readdirSync(staticJson), function(file, callback) {
      fs.readFile(staticJson + file, 'utf8', function(err, data) {
         if (err) {
            return console.log(err);
         } else {
            var postJson = {};
            postJson = JSON.parse(data);
            postJson['html'] = marked(fs.readFileSync(staticFiles + postJson.fileName, 'utf8'));

            blogJson.push(postJson);

            function dummyFunc() { callback(); } ; dummyFunc();
         }
      })
   }, function(err) {
      if (err) {
         console.log();
      } else {
         blogJson.sort(sort_by('date', false, parseInt));

         res.send(blogJson);
      }
   })
});

app.get('/info/news', function(req, res) {
   var newsJson = [];

   var staticJson = __dirname + '/static/info/news/json/';
   var staticFiles = __dirname + '/static/info/news/_source/';

   async.forEach(fs.readdirSync(staticJson), function(file, callback) {
      fs.readFile(staticJson + file, 'utf8', function(err, data) {
         if (err) {
            return console.log(err);
         } else {
            var postJson = {};
            postJson = JSON.parse(data);
            postJson['html'] = marked(fs.readFileSync(staticFiles + postJson.fileName, 'utf8'));

            newsJson.push(postJson);

            function dummyFunc() { callback(); } ; dummyFunc();
         }
      })
   }, function(err) {
      if (err) {
         console.log();
      } else {
         newsJson.sort(sort_by('date', false, parseInt));
         
         res.send(newsJson);
      }
   })
});

/*
	End
*/

app.listen(8081, 'localhost', function(){
   console.log(new Date() + ': Server listening on 8081');
});
