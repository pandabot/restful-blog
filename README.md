### A simple RESTful (api) blogging engine.

- Serves posts in json format from a RESTful server.
- Json information is read from files located in static/info/blog/json and static/info/news/json.
- Static blog or news posts are stored in static/info/blog/_source/ or static/info/news/_source/ respectively.
- Renders and returns markdown from static posts.

```
http://localhost:8081/> GET /info/blog
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 457
Etag: "-270212036"
Date: Sun, 23 Feb 2014 01:28:02 GMT
Connection: keep-alive

[
    {
        uuid: '2bf22ff2e0bb163576cc71175301311e',
        title: 'title',
        date: '1393085284',
        fileName: '2bf22ff2e0bb163576cc71175301311e',
        html: '<p><strong>Bold text</strong></p>\n<p>This is a blog entry.</p>\n'
    },
    {
        uuid: '2bf22ff2e0bb163576cc711753013c02',
        title: 'title 2',
        date: '1393095801',
        fileName: '2bf22ff2e0bb163576cc711753013c02',
        html: '<p><strong>Another blog entry</strong></p>\n'
    }
]
```

## Setup

- Installing the required modules:

```bash
$ git clone https://pandabot@bitbucket.org/pandabot/restful-blog.git
$ cd restful-blog
$ npm install async express marked
$ mkdir -p static/info/{blog,news}/{_source,archives,json}
```

More information on the ***marked*** module can be found [here](https://github.com/chjj/marked).

## Startup

```bash
node server.js
```

The json files should contain a uuid, title, date and the corresponding file name of the markdown file located in static/info/*/_source/. All of the json elements are optional except 'fileName':

```json
{ 
   "uuid": "2bf22ff2e0bb163576cc71175301311e",
   "title": "title",
   "date": "1393085284",
   "fileName": "2bf22ff2e0bb163576cc71175301311e"
}
```

The corresponding markdown file:

```markdown
**Bold text**

This is a blog entry.
```

Nginx snippet (see ./conf/example.com.conf):

```nginx
server {
   ...
   ...
   ...

   location / {
      location ~* \.(jpg|jpeg|png|gif|ico|css|js|json|woff|tff)$ {
         root        /home/user/www/example.com;
         add_header  X-Stage-Host "Stage1";

         ## Add more caching headers here ##
      }

      index       index.html;
   }

   location /info {
      add_header X-Stage-Host "Stage0";

      proxy_pass http://example.com.blog;
   }
}
```

The above configuration would result in the following:

- http://example.com/favicon.ico --> Nginx would handle this request (appending whatever headers you have added). It would be served from "/home/user/www/example.com".
- http://example.com/index.html --> ^^
- http://example.com/file.txt --> ^^
- http://example.com/info/* (Ex., /info/blog) --> This request is passed off to the node.js application on port 8080.
